<?php

namespace Drupal\layout_builder_reports\Commands;

use Drush\Commands\DrushCommands;
use Consolidation\AnnotatedCommand\AnnotationData;
use Consolidation\AnnotatedCommand\CommandData;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ModuleInstallerInterface;
use Drupal\Core\Extension\ExtensionList;

/**
 * A Drush commandfile for layout builder reports.
 */
class LayoutBuilderReportsCommands extends DrushCommands {

  protected $entityTypeManager;
  protected $entityDisplayRepository;
  protected $moduleHandler;
  protected $moduleInstaller;
  protected $extensionList;

  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    EntityDisplayRepositoryInterface $entityDisplayRepository,
    ModuleHandlerInterface $moduleHandler,
    ModuleInstallerInterface $moduleInstaller,
    ExtensionList $extensionList
    ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->entityDisplayRepository = $entityDisplayRepository;
    $this->moduleHandler = $moduleHandler;
    $this->moduleInstaller = $moduleInstaller;
    $this->extensionList = $extensionList;
  }

  // Dependency injection for services.
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_display.repository'),
      $container->get('module_handler'),
      $container->get('module_installer'),
      $container->get('extension.list.module')
    );
  }

  /**
   * Outputs a table showing whether Layout Builder is enabled for content types.
   *
   * @command layout_builder_reports:status
   * @aliases lbr-status,lbrs
   * @usage layout_builder_reports:status
   *   Show Layout Builder status for content types.
   */
  public function status() {
    $content_types = $this->entityTypeManager->getStorage('node_type')->loadMultiple();
    $display_modes = $this->entityDisplayRepository->getViewModes('node');

    // Initialize table.
    $table = [];
    $header = [
      'Content Type',
      'Display Mode',
      'Layout Builder Enabled',
      'Customizable Layout',
    ];

    // Gather data.
    foreach ($content_types as $content_type_id => $content_type) {
      foreach ($display_modes as $mode_id => $mode) {
        $entity_view_display = $this->entityTypeManager
          ->getStorage('entity_view_display')
          ->load("node.{$content_type_id}.{$mode_id}");

        if ($entity_view_display) {
          $enabled = $entity_view_display->getThirdPartySetting('layout_builder', 'enabled', FALSE);
          $overridable = $entity_view_display->getThirdPartySetting('layout_builder', 'allow_custom', FALSE);

          // Add row to table.
          $table[] = [
            'Content Type' => $content_type->label(),
            'Display Mode' => $mode['label'],
            'Layout Builder Enabled' => $enabled ? '<info>Yes</info>' : 'No',
            'Customizable Layout' => $enabled && $overridable ? 'Yes' : 'No',
          ];
        }
      }
    }

    // Render the table.
    if (!empty($table)) {
      $this->io()->table($header, $table);
    } else {
      $this->io()->warning("No content types found or no view modes with Layout Builder enabled.");
    }
  }

/**
 * Outputs a table of all modules that require Layout Builder.
 *
 * @command layout_builder_reports:dependency-modules
 * @aliases lbrdeps
 * @option installed Show only installed modules.
 * @option uninstalled Show only uninstalled modules.
 * @usage layout_builder_reports:dependency-modules
 *   Show modules that have Layout Builder as a dependency.
 * @usage layout_builder_reports:dependency-modules --installed
 *   Show only installed modules that have Layout Builder as a dependency.
 * @usage layout_builder_reports:dependency-modules --uninstalled
 *   Show only uninstalled modules that have Layout Builder as a dependency.
 */
public function dependencyModules($options = ['installed' => FALSE, 'uninstalled' => FALSE]) {
  $modules_with_dependency = [];
  $all_modules = $this->extensionList->getList();

  foreach ($all_modules as $module) {
    $info = $module->info;
    if (isset($info['dependencies']) && in_array('drupal:layout_builder', $info['dependencies'])) {
      $is_installed = $this->moduleHandler->moduleExists($module->getName());
      if (($options['installed'] && !$is_installed) || ($options['uninstalled'] && $is_installed)) {
        continue;
      }

      $modules_with_dependency[] = [
        'Module Name' => $info['name'] ?? $module->getName(),
        'Module Machine Name' => $module->getName(),
        'Installed' => $is_installed ? 'Yes' : 'No',
      ];
    }
  }

  if (!empty($modules_with_dependency)) {
    $header = ['Module Name', 'Module Machine Name', 'Installed'];
    $this->io()->table($header, $modules_with_dependency);
  } else {
    $this->io()->success("No modules matching the criteria have Layout Builder as a dependency.");
  }
}


}
