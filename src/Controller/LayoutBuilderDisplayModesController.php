<?php

declare(strict_types=1);

namespace Drupal\layout_builder_reports\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller for Content Type report.
 */
class LayoutBuilderDisplayModesController extends ControllerBase {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity display repository service.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * Constructs a new LayoutBuilderReportsController object.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, EntityDisplayRepositoryInterface $entity_display_repository) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityDisplayRepository = $entity_display_repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_display.repository')
    );
  }

  /**
   * Lists content types, their view modes, and if Layout Builder is enabled.
   *
   * @return array
   *   A render array representing the reporting page.
   */
  public function report(): array {
    $build = [];
    $build['#attached']['library'][] = 'layout_builder_reports/display_modes';

    $build['summary'] = [
      '#type' => 'markup',
      '#markup' => $this->t('This page shows the display modes for each content type and whether Layout Builder is enabled for that display mode. If Layout Builder is enabled, it also shows whether custom layouts are allowed.'),
    ];

    $content_types = $this->entityTypeManager->getStorage('node_type')->loadMultiple();
    $display_modes = $this->entityDisplayRepository->getViewModes('node');
    $display_types = $this->entityTypeManager->getStorage('entity_view_display');

    foreach ($content_types as $content_type_id => $content_type) {
      $header = [
        'display_mode' => $this->t('Display Mode'),
        'layout_builder_enabled' => $this->t('Layout Builder Enabled?'),
        'allow_custom' => $this->t('Allow Custom Layouts?'),
      ];
      $rows = [];

      // Handle the default view mode, which would otherwise not be listed.
      $default_display = $display_types->load("node.{$content_type_id}.default");
      if (isset($default_display)) {
        // Prepend the 'default' view mode to the beginning of the array.
        $display_modes = ['default' => ['label' => $this->t('Default')]] + $display_modes;
      }

      foreach ($display_modes as $mode_id => $mode) {
        $display = $mode_id === 'default' ? $default_display : $display_types->load('node.' . $content_type_id . '.' . $mode_id);
        if (isset($display)) {
          $enabled = $display->isLayoutBuilderEnabled();
          $display_mode_path = Url::fromRoute('entity.entity_view_display.node.view_mode', [
            'node_type' => $content_type_id,
            'view_mode_name' => $mode_id,
          ]);
          $allow_custom = $enabled && $display->isOverridable();

          $rows[] = [
            'display_mode' => Link::fromTextAndUrl($mode['label'], $display_mode_path),
            'layout_builder_enabled' => [
              'data' => $enabled ? $this->t('<span>Yes</span>') : $this->t('No'),
              'class' => $enabled ? ['layout-builder-enabled'] : [],
            ],
            'allow_custom' => $allow_custom ? $this->t('Yes') : $this->t('No'),
          ];
        }
      }

      $build[$content_type_id] = [
        '#type' => 'details',
        '#title' => $content_type->label(),
        '#open' => TRUE,
        'table' => [
          '#type' => 'table',
          '#header' => $header,
          '#rows' => $rows,
          '#attributes' => ['class' => ['layout-builder-overview-table']],
          '#sticky' => TRUE,
        ],
      ];
    }

    return $build;
  }

}
